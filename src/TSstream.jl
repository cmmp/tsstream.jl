module TSstream

#using Debug 
using Base.Collections

import TSanalysis.ami_calc
import TSanalysis.acf_calc
import TSanalysis.adx_calc
import Base.show
    
export cluster

include("BinaryTree.jl")
include("deviation.jl")
include("entropy.jl")
include("findbest.jl") 
include("util.jl")

function cluster(dados::Matrix{Float64}, measures::Vector{ASCIIString}, expected_labels::Any; 
                 w_length::Int64 = 200, 
                 ncoefs::Int64 = 20, periodic_eval::Bool = false,
                 eval_metric::String = "ARI", verbose::Bool = false, 
                 print_agg_split::Bool = false, alpha::Float64 = 0.3, 
                 beta::Float64 = 0.3, lambda::Float64 = 0.5,
                 eta::Float64 = 0.5, tol::Float64 = 0.05,
                 quiet::Bool = false, adapt_wlen::Bool = true)
    orig_wlen = w_length
    nseries, nobs = size(dados)
    info_lag = ceil(nobs / 20.)
    start = 1
    finish = w_length
    ari_results = zeros(0)

    local root::Node 
    firstPass = true
    clusterings = {}

    while finish <= nobs
        globalAggSplitFlag = false
        i = finish
        block_i = dados[:,start:finish]
        start = finish + 1

        vred = zeros(0) ; vredper = zeros(0); vagg = zeros(0); vaggper = zeros(0)

        inputdf = ref(Matrix{Float64})

        if !isempty(intersect(["AMI"], measures))
            inputdf = ami_calc(block_i, ncoef = ncoefs)
        end

        if !isempty(intersect(["ACF"], measures))
            acf = acf_calc(block_i, ncoef = ncoefs)
            inputdf = isempty(inputdf) ? acf : [inputdf acf]
        end

        if !isempty(intersect(["ADX"], measures))
            adx = adx_calc(block_i)
            inputdf = isempty(inputdf) ? adx : [inputdf adx]
        end

        # do the z-score scaling:
        for idx = 1:(size(inputdf)[2])
            s = inputdf[:,idx]
            s = (s - mean(s)) / std(s)
            inputdf[:,idx] = s
        end

        if firstPass
            firstPass = false
            splitAtt , bestWsd, splitVal = findBestAttribute(inputdf, beta)
            root = Node([1:nseries], splitAtt, splitVal) # create root
            if splitAtt != -1
                create_children(inputdf, root)
            end
            turn_off_memberships(root) # turn off all memberships
            push_clusterings(inputdf, root, clusterings)
        else
            ### clustering phase: ###
            turn_off_memberships(root) # turn off all memberships
            # cluser according to current structure:
            push_clusterings(inputdf, root, clusterings)
            # model update phase: traverse leaves and check agg/split
            leaves = {}
            get_leaves(root, leaves)
            while !isempty(leaves)
                curr::Node = shift!(leaves)
                aggFlag = false
                if !is_root(curr)
                    sib = getSibling(curr)
                    parent = curr.parent
                    # check for aggregation first:

                    # get the variance on the split dimension:
                    spidx = parent.att
                    varParent = mystd(inputdf[parent.members,spidx])
                    nleft = length(parent.left.members)
                    nright = length(parent.right.members)
                    sdleft = mystd(inputdf[parent.left.members,spidx])
                    sdright = mystd(inputdf[parent.right.members,spidx])
                    sdnew = (nleft * sdleft + nright * sdright) / (nleft + nright) 

                    if sdnew >= lambda * varParent # aggregate
                        aggFlag = true
                        globalAggSplitFlag = true
                        push!(vagg, sdnew)
                        push!(vaggper, sdnew / varParent)
                        aggregate_children(curr)
                        remove_sib_from_leaves(sib, leaves)
                        if print_agg_split && !quiet
                            println("Aggregation of nodes $(curr.id) and $(sib.id)...")
                        end
                    else
                        if sdnew >= (lambda - tol) * varParent
                            push!(vagg, sdnew)
                            push!(vaggper, sdnew / varParent)
                        end
                    end
                end

                if !aggFlag # no aggregation, check for splitting
                    bestAtt, bestWsd, bestSplit = findBestAttribute(inputdf[curr.members,:], beta)
                    if bestAtt == -1
                        remove_sib_from_leaves(curr, leaves)
                        continue
                    end
                    lmem = find(inputdf[curr.members,bestAtt] .<= bestSplit)
                    rmem = find(inputdf[curr.members,bestAtt] .> bestSplit)
                    nleft = length(lmem)
                    nright = length(rmem)
                    if nleft > 1 && nright > 1
                        sdleft = std(inputdf[lmem,bestAtt])
                        sdright = std(inputdf[rmem,bestAtt])
                        sdnew = (nleft * sdleft + nright * sdright) / (nleft + nright)
                        leavesd = std(inputdf[curr.members,bestAtt])
                        if sdnew < alpha * leavesd 
                            if print_agg_split && !quiet
                                println("Splitting leaf $(curr.id)...")
                            end
                            globalAggSplitFlag = true
                            push!(vred, sdnew)
                            push!(vredper, sdnew / leavesd)
                            curr.att = bestAtt
                            curr.splitVal = bestSplit
                            lchild = Node(curr, lmem)
                            rchild = Node(curr, rmem)
                            curr.left = lchild
                            curr.right = rchild
                        else
                            if sdnew < (alpha + tol) * leavesd
                                push!(vred, sdnew)
                                push!(vredper, sdnew / leavesd)
                            end
                        end
                    end
                end # end !aggFalg -- check for splitting
                remove_sib_from_leaves(curr, leaves)
            end # end while !isempty(leaves)
        end # end of else block -- there is a clustering structure already.
        
        # adapt parameters:
        if length(vred) >= 1 && sum(vred) > 0
            alphanew = sum(vred .* vredper) / sum(vred) * eta + alpha * (1. - eta)
            if !quiet
              println("Updating alpha from $alpha to $alphanew...")
            end
            alpha = alphanew
        end

        if length(vagg) >= 1 && sum(vagg) > 0
            lambdanew = sum(vagg .* vaggper) / sum(vagg) * eta + lambda * (1. - eta)
            if !quiet
                println("Updating lambda from $lambda to $lambdanew...")
            end
            lambda = lambdanew
        end

        if adapt_wlen
            if globalAggSplitFlag
                # decrease the window size
                decsize = int(w_length * 0.1)
                if w_length - decsize >= orig_wlen 
                    w_length -= decsize
                    if !quiet
                        println("Decreasing window size to $w_length...")
                    end
                end
            else
                incsize = int(w_length * 0.1)
                w_length += incsize
                if !quiet
                    println("Increasing window size to $w_length...")
                end
            end
        end
        finish += w_length
    end # end of while finish <= nobs

    return clusterings
end # end of function cluster

end # end of module
