function entropy(x::Vector{Float64}; nbins::Int64 = 10)
    if length(x) < 2
        return 0
    end
    
    h = nonzeros(hist(x, nbins)[2])
    h /= sum(h)

    return -sum(h .* log2(h))
end


