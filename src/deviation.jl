function mystd(x::Vector{Float64})
    if length(x) <= 1
        return 0
    end
    return std(x)
end

function weightedsd(x::Vector{Float64})
    # return is (weighted_sd, split_point)
    m = length(x)
    if m < 4
        return (Inf,x[1])
    end

    xs = sort(x)
    minsd = Inf
    bestSplit = -1.

    stpt1 = 1
    wsd = 0.

    for i=2:(m-2)
        endpt1 = i
        stpt2 = i + 1
        pt1 = xs[stpt1:endpt1]
        pt2 = xs[stpt2:m]
        if pt1[end] == pt2[1] # dont split where endpoints are equal
            continue
        end
        sd1 = std(pt1)
        sd2 = std(pt2)
        wsd = (i * sd1 + (m-i) * sd2) / m
        if wsd < minsd
            minsd = wsd
            bestSplit = (pt1[end] + pt2[1]) / 2.0 # pick midpoint
        end
    end
    return (minsd, bestSplit)
end
