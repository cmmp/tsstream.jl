node_id = 0 # global variable for assigning node ids

function get_next_id()
    global node_id::Int64 += 1
    return node_id::Int64
end

type Node 
    left::Node
    right::Node
    att::Int64
    splitVal::Float64
    members::Vector{Int64}
    parent::Node
    id::Int64

    # for creating a leaf:
    function Node(parent::Node, members::Vector{Int64})
        this = new()
        this.left = this.right = this
        this.members = members
        this.parent = parent
        this.id = get_next_id()
        return this
    end

    # for creating the root:
    function Node(members::Vector{Int64}, att::Int64, splitVal::Float64)
        this = new()
        this.parent = this.left = this.right = this
        this.members = members
        this.att = att
        this.splitVal = splitVal
        this.id = get_next_id()
        return this
    end
end

function aggregate_children(n::Node)
    par = n.parent
    par.left = par.right = par
    return
end

function create_children(inputdf::Matrix{Float64}, parent::Node)
    leftmem = find(inputdf[parent.members,parent.att] .<= parent.splitVal)
    rightmem = find(inputdf[parent.members,parent.att] .> parent.splitVal)
    if length(leftmem) < 2 || length(rightmem) < 2
        error("Splitting a node into children with less than 2 members!")
    end
    lchild = Node(parent, leftmem)
    rchild = Node(parent, rightmem)
    parent.left = lchild
    parent.right = rchild
    return
end

function getSibling(n::Node)
    if is_root(n)
        error("Trying to get sibling of root...")
    end
    if is(n.parent.left, n) # this is the left sibling
        return n.parent.right
    else
        return n.parent.left
    end
end

function turn_off_memberships(root::Node)
    if has_left_child(root)
        turn_off_memberships(root.left)
    end
    if has_right_child(root)
        turn_off_memberships(root.right)
    end
    root.members = zeros(0)
end

function get_leaves(root::Node, leaves::Array{Any})
    if has_left_child(root)
        get_leaves(root.left, leaves)
    end
    if has_right_child(root)
        get_leaves(root.right, leaves)
    end
    if is_leaf(root)
       push!(leaves,root)
    end
end

function push_clusterings(inputdf::Matrix{Float64}, root::Node, clusterings::Array{Any})
    m, n = size(inputdf)
    memberships = zeros(Int64, m)
    for i = 1:m
        curr::Node = root
        while !is_leaf(curr)
            push!(curr.members, i)
            if inputdf[i,curr.att] <= curr.splitVal
                curr = curr.left
            else
                curr = curr.right
            end
        end
        push!(curr.members, i)
        memberships[i] = curr.id
    end
    push!(clusterings, memberships) 
end

function is_root(n::Node)
    return is(n, n.parent)
end

function is_leaf(n::Node)
    return !has_left_child(n) && !has_right_child(n)
end

function has_left_child(n::Node)
    return !is(n, n.left)
end

function has_right_child(n::Node)
    return !is(n, n.right)
end

function count_nodes(root::Node)
    count::Int64 = 0
    if has_left_child(root)
        count += count_nodes(root.left)
    end
    if has_right_child(root)
        count += count_nodes(root.right)
    end
    return count + 1
end

function show(io::IO, n::Node)
    count = count_nodes(n)
    print(io, "TS-Stream (sub)tree with $count node(s).")
end

