function findBestAttribute(X::Matrix{Float64}, beta::Float64)
    maxgain = -Inf
    bestAtt = -1
    bestWsd = -1.
    bestSplit = -1.

    m, n = size(X) # m -> num. series ; n -> num. attrs

    if m == 0
        return (bestAtt, bestWsd, bestSplit)
    end

    for i = 1:n
       if entropy(X[:,i])  < beta
           continue
       end

       ganho = mystd(X[:,i])
       wsd, bsplit = weightedsd(X[:,i])
       ganho = ganho - wsd
       if ganho > maxgain
           maxgain = ganho
           bestAtt = i
           bestWsd = wsd
           bestSplit = bsplit
       end
    end

    return (bestAtt, bestWsd, bestSplit)
end
