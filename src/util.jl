function remove_sib_from_leaves(n::Node, leaves::Array{Any})
    for i=1:length(leaves)
        if is(leaves[i], n)
            splice!(leaves, i)
            return
        end
    end
end
