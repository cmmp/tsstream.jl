# test binary tree creation:
using Base.Test
using TSstream

inputdf = float([1 2 3 4 5 ; 6 7 8 9 10]')
m, n = size(inputdf)
root = TSstream.Node([1:m], 2, 7.5)
TSstream.create_children(inputdf, root)

@test TSstream.count_nodes(root) == 3
@test TSstream.is_root(root) == true
@test TSstream.is_leaf(root.left) == true
@test TSstream.is_leaf(root.right) == true
@test TSstream.has_left_child(root) == true
@test TSstream.has_right_child(root) == true
@test TSstream.has_left_child(root.left) == false
@test TSstream.has_right_child(root.left) == false
@test TSstream.has_left_child(root.right) == false
@test TSstream.has_right_child(root.right) == false
@test is(root.left, TSstream.getSibling(root.right)) == true
@test is(root.right, TSstream.getSibling(root.left)) == true

