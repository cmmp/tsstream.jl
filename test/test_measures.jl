using Base.Test
using TSstream
using ClusterAnalysis

dados = readdlm("series.dat", ' ', has_header = false)
supervisao = vec(int(readdlm("supervisao.dat", ' ', has_header = false)))
ret = TSstream.cluster(dados, ["AMI", "ACF"], supervisao, quiet = true)

medias = zeros(0)
for clu in ret
    ari = ClusterAnalysis.adjusted_rand_index(clu, supervisao)
    push!(medias, ari)
end

@test mean(medias) >= 0.8
