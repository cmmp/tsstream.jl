# testing entropy:
using Base.Test
using TSstream

x = vec(float(vcat(repmat([1],10,1), repmat([2],10,1)))) 
@test 1.0 == TSstream.entropy(x, nbins = 2)

x = vec(repmat([1.], 30, 1))
@test 0.0 == TSstream.entropy(x, nbins = 2)

