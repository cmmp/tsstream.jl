# testing weightedsd:
using Base.Test
using TSstream

x = [1.,2,3,20,30,40]

expsd = (1.0 * 3 + 10.0 * 3) / 6.
expcut = (3.0 + 20) / 2.

sdgot, cutgot = TSstream.weightedsd(x)

@test expsd == sdgot

@test expcut == cutgot

